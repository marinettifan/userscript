// ==UserScript==
// @name         Qobuz Full Track Player
// @version      0.3.4
// @description  Replace the preview track button with a button that plays the entire track on Qobuz.
// @author       Your Name
// @match        https://www.qobuz.com/*/album/*/*
// @match        https://www.qobuz.com/*/playlists/*/*
// @match        https://marinettifan.codeberg.page/
// @icon         https://www.qobuz.com/favicon.ico
// @grant        GM.getValue
// @grant        GM.setValue
// @grant        GM_openInTab
// @grant        GM_addElement
// @grant        GM_addStyle
// @require      https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.1.1/core.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.1.1/md5.min.js
// @require      https://raw.codeberg.page/marinettifan/vorbjs/FLACMetadataEditor.min.js
// @require      https://raw.codeberg.page/marinettifan/uzip-minified/uzip.min.js
// ==/UserScript==
//

(async function() {
  'use strict';
  let tk;
  var tokenCheck = JSON.parse((tk = await GM.getValue('TOKENS')) ? tk : null);
  if (tokenCheck == null && window.location.hostname != "marinettifan.codeberg.page") {
    await GM_openInTab("https://marinettifan.codeberg.page");
  }

  const QOBUZ_API_URL="https://www.qobuz.com/api.json/0.2",
        APP_ID='950096963',
        TOKENS=tokenCheck,
        SECRET='979549437fcc4a3faad4867b5cd25dcb';
  var tokens = TOKENS;

  async function getFullURI(trackId, passthru) {
    let timestamp = Math.floor(Date.now()/1000),
        requestsig = "trackgetFileUrlformat_id27intentstreamtrack_id"+trackId+timestamp+SECRET,
        hash = CryptoJS.MD5(requestsig).toString(),
        URI = QOBUZ_API_URL+"/track/getFileUrl?format_id=27&intent=stream&track_id="+trackId+
        "&request_ts="+timestamp+
        "&request_sig="+hash+
        "&user_auth_token="+tokens[0]+
        "&app_id="+APP_ID;

    let returnURL, code;
    function handleresponse(response) {
      returnURL = response[0];
      code = response[1].code;
    }
    function handleerror(error) {
      console.error("Error occurred in getFullURI: code", error);
      returnURL = -1;
    }

    return fetch(URI)
      .then (response => response.ok ? response.json() : Promise.reject(response))
      .then(async data => {
        if ((data.url == null || (data.restrictions != null && (data.restrictions[0].code == "UserUncredentialed" || data.restrictions[0].code == "UserUnauthenticated"))) && (passthru == null || passthru == false)) {
          while ((returnURL == null || code == "UserUncredentialed" || code == "UserUnauthenticated") && tokens.length != 0) {
            tokens.shift();
            await getFullURI(trackId, true)
              .then(handleresponse)
              .catch(handleerror);
          }
          if(returnURL != -1) {
            return [returnURL, code];
          } else {
            return Promise.reject(-1);
          }
        } else {
          return [data.url, data.restrictions == null ? null : data.restrictions[0]];
        }
    })
      .catch(error => {
        console.error("Error occurred in fetch: code", error);
        return Promise.reject(-1);
    });
  }

  async function processtrack(item) {
      const duration = item.getElementsByClassName("track__item--duration")[0].innerHTML;
      var seconds = 0;
      for (var i = 0; i < duration.split(":").reverse().length; ++i) {
        seconds+= Number(duration.split(":").reverse()[i]) * (60**i);
      }
      var trackId = item.getAttribute("data-track");

    return new Promise(async function(resolve, reject) {
      await getFullURI(trackId)
        .then(function(response) {
            item.setAttribute("data-source",response[0]);
            // decisecond
            item.setAttribute("data-duration",seconds*10);
            item.querySelector(".track__player").classList.add("track__loaded");
            resolve(0);
        })
        .catch(function(error) {
            console.error("Error occurred in processtrack: code", error);
            reject(error);
        });
    });
  }

  if (window.location.hostname == "marinettifan.codeberg.page") {
    window.addEventListener('load', async function() {
      document.querySelector("button#userscript").addEventListener("click",async function() {
        let tokenArray = document.querySelector("#tokensOutput").value;
        await GM.setValue("TOKENS", tokenArray);
        tokenCheck = tokenArray;
      });
    });
  } else {
    // Wait for the page to load before modifying the button
    window.addEventListener('load', async function() {
      let test;
      const albumtitle = document.querySelector(".album-meta .album-meta__title").textContent,
            artist = (test = document.querySelector("[href*='/interpreter/'].album-about__item--link")) ? test.textContent : "Various",
            label = (test = document.querySelector("[href*='/label/'].album-about__item--link")) ? test.textContent : "Various",
            composer = (test = document.querySelectorAll("[href*='/composer/'].album-about__item--link")) ? test.textContent : "Various",
            genres = [...new Set([...document.querySelectorAll("[href*='/genre/'].album-about__item--link")].map(item => item.textContent).join("/").split("/"))],
            released = JSON.parse(document.querySelector("[type='application/ld+json']").textContent).releaseDate,
            tracktotal = document.querySelectorAll(".track").length,
            artwork = [{
              src: document.querySelector(".album-cover img").src,
              sizes: (document.querySelector(".album-cover img").naturalWidth+"x"+document.querySelector(".album-cover img").naturalHeight),
              type: ("image" + "/" + document.querySelector(".album-cover img").src.split(/[./]/).slice(-1)[0])
            }];

      document.querySelector(".album-player .player #player").setAttribute("preload","auto");
      var NodeArray = [...document.querySelectorAll('.player__item .player__tracks .track')];
      NodeArray = NodeArray.filter(item => item.querySelector(".track__player") != null ? item : false);

      let firsttrack = NodeArray.shift();
      await processtrack(firsttrack);
      let promises = NodeArray.map((item) => processtrack(item));
      await Promise.allSettled(promises);
      NodeArray = [...document.querySelectorAll('.player__item .player__tracks .track')];
      NodeArray = NodeArray.filter(item => item.querySelector(".track__player") != null ? item : false);

      GM_addStyle(`.pct-download {
                    --green-color: #3BB322;
                    --green-color-hover: #3FBC25;
                    font-size: unset !important;
                    width: unset !important;
                  }
                  .pct-download:hover::before {
                    background: var(--green-color-hover) !important;
                  }
                  .pct-download::before {
                    background: var(--green-color) !important;
                    content: "\\2B73" !important;
                    font-size: 1.39em;
                    border-radius: 50%;
                    display: inline-block;
                    width: 200%;
                    height: 0;
                    padding-bottom: 200%;
                    text-align: center;
                    cursor: pointer;
                  }`);
      // metadata is an array containing any of the following
      // "Performers", "Title", "Tracknumber", "Copyright"
      function gtm(item, metawanted) {
        let metadata = {};
        let dataset = item.dataset;
        let trackV2 = JSON.parse(item.dataset.trackV2);
        if (! metawanted) {
          metawanted = ["Performers", "Title", "Tracknumber", "Copyright"];
        }
        metawanted.forEach(meta => {
          switch(meta) {
            case "Performers":
              let perform = {};
              let performers = item.querySelector(".track__infos").children[0].textContent.replace(/[\n\r]/g, "").split(" - ").map(item => item.split(", "));
              function process(person, item) {
                    item = item.replace("MainArtist", "Artist").replace(" ", "_");
                    item = item.toUpperCase();
                    perform[item] = perform[item] ? [].concat(perform[item], person) : person;
              }
              for (let x of performers) {
                if (x.length > 2) {
                  let bound = process.bind(null, x.shift());
                  x.forEach(bound);
                } else if (x.length == 2) {
                  let bound = process.bind(null, x.shift());
                  bound(x[0]);
                }
              }
              metadata.Performers = perform;
              break;
            case "Title":
              metadata.Title = trackV2.item_name;
              break;
            case "Tracknumber":
              metadata.Tracknumber = Number(dataset.index) + 1;
              break;
            case "Copyright":
              metadata.Copyright = item.querySelector(".track__infos").children[1].textContent;
              break;
          }
        });
        return metadata;
      }

      async function fetchwithProgress(item, source) {
        let response = await fetch(source);
        let contentLength = response.headers.get('content-length');
        let total = parseInt(contentLength, 10);
        let loaded = 0;
        let progress = GM_addElement(item.querySelector(".track__item--name"), "progress", {id: "fetchp", max: 100, value: 0, style: "accent-color: #3BB322; width: 120%"});
        const res = new Response(new ReadableStream({
            async start(controller) {
              const reader = response.body.getReader();
              for (;;) {
                const {done, value} = await reader.read();
                if (done) break;
                loaded += value.byteLength;
                progress.value = Math.round((loaded/total)*100);
                controller.enqueue(value);
              }
              controller.close();
            },
        }));
        let blob = await res.blob();
        item.querySelector(".track__item--name").removeChild(progress);
        return blob;
      }
      async function tag(item) {
        let metadata = await gtm(item);
        let track = await fetchwithProgress(item, item.dataset.source);
        track = await track.arrayBuffer();
        let editor = new FLACMetadataEditor(track);
        editor.addComment("COPYRIGHT", metadata.Copyright);
        editor.addComment("TRACKNUMBER", metadata.Tracknumber);
        editor.addComment("TITLE", metadata.Title);
        editor.addComment("ALBUM", albumtitle);
        editor.addComment("LABEL", label);
        editor.addComment("DATE", released);
        editor.addComment("YEAR", released.split("-")[0]);
        editor.addComment("TRACKTOTAL", tracktotal);
        genres.forEach(genre => editor.addComment("GENRE",genre));
        function addcomment (key, item) {
          editor.addComment(key, item);
        }
        for (let key in metadata.Performers) {
          if (typeof(metadata.Performers[key]) != "string") {
            let bound = addcomment.bind(null, key);
            metadata.Performers[key].forEach(bound);
          } else {
            editor.addComment(key, metadata.Performers[key]);
          }
        }
        let picture = await fetch(artwork[0].src);
        picture = await picture.arrayBuffer();
        editor.addPicture({
          APICtype: 3,
          MIMEType: artwork[0].type,
          colorDepth: 0,
          colorNumber: 0,
          data: picture,
          description: 'Cover (front)',
          width: artwork[0].sizes.split("x")[0],
          height: artwork[0].sizes.split("x")[1]
        });
        editor.serializeMetadata();
        let uintbuffer = new Uint8Array(editor.arrayBuffer);
        return [uintbuffer, metadata.Title];
      }
      function dlblob(blob, name) {
          let url = URL.createObjectURL(blob);
          let a = document.createElement('a');
          a.href = url;
          a.download = name;
          a.click();
          URL.revokeObjectURL(url);
      }

      // these functions are dispatched through events
      async function tagdl(event, all) {
        if (all == null) {
          let button = event.currentTarget;
          let item = button.parentElement.parentElement;
          let resultArray = await tag(item);
          let resultArrayBuffer = resultArray[0];
          let blob = new Blob([resultArrayBuffer], {type: 'audio/flac'});
          dlblob(blob, resultArray[1]);
        } else if (all == true) {
          let flacArray = {};
          let resultArray = [];
          let promises = NodeArray.map((item) => tag(item));
          await Promise.allSettled(promises)
            .then(data => data.forEach(object => resultArray.push(object.value)));
          resultArray.forEach(item => {
            flacArray[item[1]+".flac"] = item[0];
          });
          let zip = UZIP.encode(flacArray);
          let blob = new Blob([zip]);
          dlblob(blob, albumtitle + ".zip");
        }
      }
      function setattr(event){
        let element = event.currentTarget;
        element.removeEventListener("ended",setattr);
        play(element.array[0], element.array[1], 1);
      }

      function metadatadel(event) {
        let element = event.currentTarget;
        if (element.metadataset == 0) {
          navigator.mediaSession.setActionHandler("previoustrack",null);
          navigator.mediaSession.setActionHandler("nexttrack",null);
          navigator.mediaSession.metadata = null;
        }
        element.metadataset = 0;
      }
      function startorstop() {
        let element = document.querySelector(".track--paused .track__player, .track--playing .track__player");
        if (! element) {
          document.querySelector(".track .track__player").click();
        } else {
          element.click();
        }
      }

      // end of event functions
      // norp: 1 = next, 0 = prev
      function play(NodeArray, index, norp) {
        if (norp == 1) {
          let nextelement = NodeArray[(index+1) % NodeArray.length];
          nextelement.querySelector(".track__player").click();
        } else if (norp == 0) {
          // (index+NodeArray.length-1) == (index-1)%NodeArray.length + 0
          // we need the the expression to be positive
          let prevelement = NodeArray[(index+NodeArray.length-1) % NodeArray.length];
          prevelement.querySelector(".track__player").click();
        }
      }

      if (document.querySelector(".album-cover__buttons .pct-play") != null) {
        let referenceNode = document.querySelector(".album-cover__buttons .pct-play");

        let newNodePrev = document.createElement("button");
        newNodePrev.className = "album-cover__button pct-prev";
        let newNodeNext = document.createElement("button");
        newNodeNext.className = "album-cover__button pct-next";

        GM_addStyle(`.pct-prev::before {
                      content: '\u23EE';
                    }
                    .pct-next::before {
                      content: '\u23ED';
                    }
                    .pct-next::before, .pct-prev::before {
                      font-size: 18px;
                    }`);

        newNodePrev.addEventListener("click", function(){
          let player = document.querySelector(".album-player .player #player");
          play(player.array[0], player.array[1], 0);
        });
        newNodeNext.addEventListener("click", function(){
          let player = document.querySelector(".album-player .player #player");
          play(player.array[0], player.array[1], 1);
        });

        referenceNode.parentNode.insertBefore(newNodePrev, referenceNode);
        referenceNode.parentNode.insertBefore(newNodeNext, referenceNode.nextElementSibling );
      }

      GM_addStyle(`.pct-playclick {
                    --green-color: #3BB322;
                    --green-color-hover: #3FBC25;
                  }
                  .pct-playclick:hover {
                    background: var(--green-color-hover) !important;
                  }
                  .pct-playclick {
                    background: var(--green-color) !important
                  }`);

      let coverbutton = document.querySelector(".player__cover .player__button");
      coverbutton.classList.add("pct-playclick");
      coverbutton.replaceWith(coverbutton.cloneNode());
      let albumbutton = document.querySelector(".album-cover__button.pct-play");
      albumbutton.classList.add("pct-playclick");
      albumbutton.replaceWith(albumbutton.cloneNode());
      [...document.querySelectorAll(".pct-playclick")].forEach(item => item.addEventListener("click", startorstop));

      let isplaylist = window.location.pathname.search("/playlists/");
      NodeArray.forEach(function(item, index) {
        item.querySelector(".track__player").addEventListener("click",function(event){
          let shit = document.querySelector(".album-player .player #player");
          shit.array = [NodeArray, index];
        });
        if (isplaylist == -1) {
          let pct = item.querySelector(".pct-info");
          pct.classList.add("pct-download");
          pct.classList.remove("track__item--info");
          pct.addEventListener("click",tagdl);

          item.querySelector(".track__player").addEventListener("click",function(event){
            let shit = document.querySelector(".album-player .player #player");
            shit.addEventListener("ended", setattr);
            if ("mediaSession" in navigator) {
              shit.metadataset = 1;
              shit.removeEventListener("durationchange",metadatadel);
              shit.addEventListener("durationchange", metadatadel);
              let metadata = gtm(event.currentTarget.parentElement.parentElement.parentElement, ["Performers", "Title"]);
              let performers = metadata.Performers;
              performers = [...new Set([].concat(performers.COMPOSER, performers.PERFORMER, performers.ARTIST, performers.COMPOSERLYRICIST, artist).filter(item => item))].sort();
              navigator.mediaSession.metadata = new MediaMetadata({
                title: metadata.Title,
                artist:  performers.join(", "),
                album: albumtitle,
                artwork: artwork
              });
              navigator.mediaSession.setActionHandler("previoustrack", function () {
                navigator.mediaSession.setActionHandler("previoustrack",null);
                play(NodeArray, index, 0);
              });
              navigator.mediaSession.setActionHandler("nexttrack", function () {
                navigator.mediaSession.setActionHandler("nexttrack",null);
                play(NodeArray, index, 1);
              });
            }
          });
        }
      });
      if (isplaylist == -1) {
        document.querySelector(".share-popin").replaceWith(NodeArray[0].querySelector(".pct-info").cloneNode());
        document.querySelector(".player__buttons .pct-download").addEventListener("click", (event) => tagdl(event, true));
      }
    });
    GM_addStyle(`.track__loaded {
              --green-color: #3BB322;
              --green-color-hover: #3FBC25;
              }
              .track__loaded:hover {
                background:var(--green-color-hover) !important;
              }
              .track__loaded {
                background:var(--green-color) !important;
              }`);
  }
})();